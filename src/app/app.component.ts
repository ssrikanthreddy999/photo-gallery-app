import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { userService } from './user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Photo-Gallery';
  users: any;
  albums: any;
  photos: any;
  filterPhotos: any;
  filterUsers: any;
  checkedList: any = [];
  username: any;
  filterValue: any;
  // tslint:disable-next-line:no-shadowed-variable
  constructor(private userService: userService){

  }

  // tslint:disable-next-line:typedef
  ngOnInit(){
    forkJoin({
      users: this.userService.getAllUsers(),
      albums: this.userService.getAlbums(),
      photos: this.userService.getPhotos(),
    })
    .subscribe(({users, albums, photos}) => {
      this.users =  this.filterUsers =  users;
      this.albums = albums;
      this.photos = photos;
      this.filterPhotos = [];
    });
  }

  getPhotosCounts(id: number): number{
    return this.photos.filter(photos => photos.albumId === id).length;
  }

  filter_Photos(id: number, event: any): any{
    if (event.target.checked){
      this.checkedList.push(id);
    }
    else{
      for (let i = 0; i < this.checkedList.length; i++){
        if (this.checkedList[i] === id){
          this.checkedList.splice(i, 1);
        }
      }
    }

    this.filterPhotos = this.photos.filter(photos => this.checkedList.includes(photos.albumId));
  }

  filter_Users(event: any): any{
    if (event.target.value){
    this.filterUsers = this.users.filter((user: any) => {
      return user.username.toLowerCase().includes(event.target.value.toLowerCase());
      });
    }
    else{
      this.filterUsers = this.users;
    }
  }

}
