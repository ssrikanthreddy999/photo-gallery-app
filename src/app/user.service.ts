import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
// tslint:disable-next-line:class-name
export class userService {

  constructor(private http: HttpClient) { }
  getAllUsers(): any{
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  getAlbums(): any{
    return this.http.get('https://jsonplaceholder.typicode.com/albums');
  }

  getPhotos(): any{
    return this.http.get('https://jsonplaceholder.typicode.com/photos');
  }
}
